import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * @author Josip Pejicic
 *
 */
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private int photonentorpedoAnzahl;
	

	// Konstruktoren

	public Raumschiff() {

	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	// Getter und Setter

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	// Methoden
	

	/**
	 * @param neueLadung Ladung, welche dem Ladungsverzeichnis hinzugefuegt werden soll.
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * @param r Raumschiff welches getroffen wird
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (this.photonentorpedoAnzahl < 1) {
			this.nachrichtAnAlle("=*Click*=-");
		} else {
			this.photonentorpedoAnzahl--;
			this.nachrichtAnAlle("Photonentorpedo abgeschossen");
			this.treffer(r);
		}
	}

	/**
	 * @param r Raumschiff welches getroffen wird
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent < 50) {
			this.nachrichtAnAlle("=*Click*=");
		} else {
			this.energieversorgungInProzent -= 50;
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			this.treffer(r);
		}
	}

	/**
	 * @param r Raumschiff das von einem Schuss getroffen wurde
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.schiffsname + " wurde getroffen!");
		// Treffer vermerken
		r.schildeInProzent -= 50;
		if (r.schildeInProzent <= 0) {
			r.schildeInProzent = 0;
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
		}

		if (r.huelleInProzent <= 0) {
			r.huelleInProzent = 0;
			r.lebenserhaltungssystemeInProzent = 0;
			this.nachrichtAnAlle("Die Lebenserhaltungssysteme von " + r.schiffsname + " wurden vernichtet.");
		}
	}

	/**
	 * @param message Nachricht die ausgegeben wird und dem Broadcast Kommunikator hinzugefuegt wird
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println("\nNachricht an Alle:");
		System.out.println("\t" + message);
		this.broadcastKommunikator.add(message);
	}

	/**
	 * @return Gibt den Broadcast Kommunikator des Raumschiffes zurueck
	 */
	public ArrayList<String> eintraegeLobguchZurueckgeben() {
		return this.broadcastKommunikator;
	}
	
	/**
	 * @param logbuch Eine ArrayList vom Typ String, das Logbuch eines Raumschiffes
	 */
	public void logbuchAusgeben(ArrayList<String> logbuch) {
		System.out.printf("\n%s\"%s\":\n", "Logbuch: ", this.schiffsname);
		for (String s : logbuch) {
			System.out.println("\t" + s);
		}
	}
	
	/**
	 * @param anzahlTorpedos Die Anzahl and Torpedos, welche geladen werden soll.
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean check = false;

		for (int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getBezeichnung().toLowerCase().equals("photonentorpedo")) {
				if (this.getLadungsverzeichnis().get(i).getMenge() >= anzahlTorpedos) {
					check = true;
					this.photonentorpedoAnzahl = anzahlTorpedos;
					int neueMenge = this.getLadungsverzeichnis().get(i).getMenge() - anzahlTorpedos;
					this.getLadungsverzeichnis().get(i).setMenge(neueMenge);
					System.out.println(this.photonentorpedoAnzahl + " Photonentorpedo" + (this.photonentorpedoAnzahl > 1 ? "s" : "") +" eingesetzt");
					break;
				}
			}

		}

		if (check) {
		} else {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}
	/**
	 * 
	 * @param schutzschilde Prozentuale Kapazität der Schutzschilde
	 * @param energieversorgung Prozentuale Kapazität der Energieversorgung
	 * @param schiffshuelle Prozentuale Kapazität der Schiffshülle
	 * @param anzahlDroiden Anzahl der Reperatursdroiden, welche sich an Board befinden
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

		int zuReparieren = 0;
		if (schutzschilde)
			zuReparieren++;
		if (energieversorgung)
			zuReparieren++;
		if (schiffshuelle)
			zuReparieren++;
		
		// Generieren der Zufallszahl
		
		Random random = new Random();
		int r = random.nextInt(100 + 1) + 1;

		if (anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}

		if (zuReparieren != 0) {
			double reparaturInProzent = (r * anzahlDroiden) / zuReparieren;

			if (schutzschilde)
				this.schildeInProzent += reparaturInProzent;
			if (energieversorgung)
				this.energieversorgungInProzent += reparaturInProzent;
			if (schiffshuelle)
				this.huelleInProzent += reparaturInProzent;
		}

	}

	/**
	 * 
	 */
	public void zustandRaumschiff() {
		System.out.println("\nDas Schiff heisst " + this.schiffsname);
		System.out.println("Die Anzahl der Photonentorpedos betr\u00e4gt: " + this.photonentorpedoAnzahl);
		System.out.println("Die Energieversorgung betr\u00e4gt " + this.energieversorgungInProzent + "%");
		System.out.println("Die Schildst\u00e4rke betr\u00e4gt " + this.schildeInProzent + "%");
		System.out.println("Die H\u00fclle hat eine St\u00e4rke von " + this.huelleInProzent + "%");
		System.out.println("Die Lebenserhaltungssysteme haben eine St\u00e4rke von " + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Das Raumschiff hat " + this.androidenAnzahl + " Androiden");

	}
	
	/**
	 * 
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("\nLadungsverzeichnis: ");
		for (Ladung l : this.getLadungsverzeichnis()) {
			System.out.println(l);
		}
	}

	/**
	 * 
	 */
	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getMenge() == 0) {
				this.getLadungsverzeichnis().remove(i);
			}
		}
	}

}