
public class Schilde {

	private int prozent;

	
	public Schilde(int prozent) {
		
		this.prozent = prozent;
	}

	public int getProzent() {
		return prozent;
	}

	public void setProzent (int prozent) {
		this.prozent = prozent;
	}

}
