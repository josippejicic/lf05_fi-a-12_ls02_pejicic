public class Energieversorgung {

	
	private int prozent;

	
	public Energieversorgung(int prozent) {
		
		this.prozent = prozent;
	}

	public int getProzent() {
		return prozent;
	}

	public void setProzent (int prozent) {
		this.prozent = prozent;
	}

}