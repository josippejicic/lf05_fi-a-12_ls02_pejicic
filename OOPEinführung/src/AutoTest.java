import java.util.Scanner;

public class AutoTest {

	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);

		Auto a1 = new Auto();
		
		a1.setMarke("Fiat");
		System.out.println(a1.getMarke());
		a1.setFarbe("Rot");
		System.out.println(a1.getFarbe());
		System.out.println("---");
		
		//("VW", "Silber");		
		Auto a2 = new Auto();
		a2.setMarke("VW");
		System.out.println(a2.getMarke());
		a2.setFarbe("Silber");
		System.out.println(a2.getFarbe());
	}

}
