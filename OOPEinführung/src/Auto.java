import java.util.ArrayList;

public class Auto {

	private String marke;
	private String farbe;   
	private Ladung ladung;
	private Photonentorpedo photonentorpedo;
	private ArrayList<Ladung> ladungen = new ArrayList<Ladung>();
	
	public Auto() {
	}
	
	public Auto(String marke, String farbe) {
		this.marke = marke;
		this.farbe = farbe;
		this.ladung = new Ladung("schuhe", 12);
	}
	
	// Ab hier get und set. 

	public void setMarke (String marke) {
		this.marke = marke;
	}

	public String getMarke() {
		return marke;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public ArrayList<Ladung> getLadungen() {
		return ladungen;
	}

	public void setLadungen(ArrayList<Ladung> ladungen) {
		this.ladungen = ladungen;
	}

	public Ladung getLadung() {
		return ladung;
	}

	public void setLadung(Ladung ladung) {
		String schuhe = null;
		this.ladung = new Ladung(schuhe, 12);
	}

	public Photonentorpedo getPhotonentorpedo() {
		return photonentorpedo;
	}

	public void setPhotonentorpedo(Photonentorpedo photonentorpedo) {
		this.photonentorpedo = photonentorpedo;
	}

}